package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
)

var movies = Movies{
	Movie{"Sin limites", 2019, "Desconocido"},
	Movie{"Ciudadano ejemplar", 2010, "John Smith"},
	Movie{"A toda maquina", 1991, "John Travolta"},
}

func getSession() *mgo.Session {
	session, err := mgo.Dial("mongodb://localhost")
	if err != nil {
		panic(err)
	}

	return session
}

var collection = getSession().DB("curso_golang").C("movies")

func responseMovies(w http.ResponseWriter, status int, results []Movie)  {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(results)
}

func responseMovie(w http.ResponseWriter, status int, results Movie)  {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(results)
}

func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hola mundo desde mi api rest full con GO")
}

func MovieList(w http.ResponseWriter, r *http.Request) {
	//fmt.Fprintf(w, "Pagina de peliculas")
	//json.NewEncoder(w).Encode(movies) //Lo que hacemos adherir el json a la response "w"
	var results []Movie
	err := collection.Find(nil).Sort("_id").All(&results) // el parametro nil quiere decir que no ponemos ninguna condicion de busqueda y All es para decirle que saque todito. Tambien hay One
	//que es para sacar el primero
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(results)
	responseMovies(w,200, results)
}

func MovieShow(w http.ResponseWriter, r *http.Request) {
	/*params := mux.Vars(r)
	movieId := params["id"]
	fmt.Fprintf(w, "Detalles de peliculas %s", movieId)*/
	params := mux.Vars(r)
	movieId := params["id"]

	//tenemos que convertir el Id a un id hexadecimal de json binario
	if !bson.IsObjectIdHex(movieId) {
		w.WriteHeader(404)
		return
	}

	oid := bson.ObjectIdHex(movieId)
	fmt.Println(oid)
	results := Movie{}
	err := collection.FindId(oid).One(&results) // el parametro nil quiere decir que no ponemos ninguna condicion de busqueda y All es para decirle que saque todito. Tambien hay One
	//que es para sacar el primero
	if err != nil {
		w.WriteHeader(404)
		return
	}

	fmt.Println(results)
	responseMovie(w,200, results)
}

func MovieAdd(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)//Lo que va a recibir por post los datos en el body

	//Ahora lo convertiremos en un objeto que podamos usar
	var movie_data Movie //movie_data es de tipo movie (ver fichero movie.go)
	err := decoder.Decode(&movie_data)//Metemos lo descifrado del body en la variable movie_data

	if err != nil {
		panic(err)
	}

	defer r.Body.Close()
	//log.Println(movie_data)
	error := collection.Insert(movie_data)
	if error != nil {
		w.WriteHeader(500)
		return
	}
	responseMovie(w,200, movie_data)
	/*w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(movie_data)//Solo codificamos lo que hemos guardado e IMPORTANTE ponerla la ultima
	//movies = append(movies, movie_data)*/
}

func MovieUpdate(w http.ResponseWriter, r *http.Request) {
	/*params := mux.Vars(r)
	movieId := params["id"]
	fmt.Fprintf(w, "Detalles de peliculas %s", movieId)*/
	params := mux.Vars(r)
	movieId := params["id"]

	//tenemos que convertir el Id a un id hexadecimal de json binario
	if !bson.IsObjectIdHex(movieId) {
		w.WriteHeader(404)
		return
	}

	oid := bson.ObjectIdHex(movieId)
	decoder := json.NewDecoder(r.Body)

	var movie_data Movie
	err := decoder.Decode(&movie_data)

	if err != nil {
		panic(err)
		w.WriteHeader(500)
		return
	}
	defer r.Body.Close()

	document := bson.M{"_id": oid}

	change := bson.M{"$set": movie_data}
	err = collection.Update(document, change)

	if err != nil {
		w.WriteHeader(404)
		return
	}

	responseMovie(w, 200, movie_data)
}

type Message struct {
	Status string `json:"status"`
	Message string `json:"message"`
}

func (this *Message) setStatus(data string){
	this.Status = data
}

func (this *Message) setMessage(data string){
	this.Message = data
}

func MovieDelete(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	movieId := params["id"]

	//tenemos que convertir el Id a un id hexadecimal de json binario
	if !bson.IsObjectIdHex(movieId) {
		w.WriteHeader(404)
		return
	}

	oid := bson.ObjectIdHex(movieId)

	err := collection.RemoveId(oid)

	if err != nil {
		w.WriteHeader(404)
		return
	}

	//results := Message{"success", "La pelicula "+movieId+" ha sido borrada correctamentes!!!!"}
	message := new(Message)
	message.setMessage("La pelicula "+movieId+" ha sido borrada correctamentes!!!!")
	message.setStatus("200")
	results := message
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	json.NewEncoder(w).Encode(results)
	//responseMovie(w, 200, "movie_data")
}
