package main

type Movie struct {
	Name string `json:"name"` //Tambien se puede utilizar esta técnica para poner los nombres en otro idioma Ej: `json:"nombre"`
	Year int `json:"year"`
	Director string `json:"director"`
}

type Movies []Movie
