package main

import (
	"github.com/gorilla/mux"
	"net/http"
)

type Route struct {
	Name string
	Method string
	Pattern string
	HandleFunc http.HandlerFunc
}

type Routes []Route

func NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)

	for _, route := range routes{
		router.Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(route.HandleFunc)
	}

	return router
}

var routes = Routes{
	{
		"Index",
		"GET",
		"/",
		Index,
	},
	{
		"MovieList",
		"GET",
		"/peliculas",
		MovieList,
	},
	{
		"MovieShow",
		"GET",
		"/pelicula/{id}",
		MovieShow,
	},
	{
		"MovieAdd",
		"Post",
		"/pelicula",
		MovieAdd,
	},
	{
		"MovieUpdate",
		"PUT",
		"/peliculaupdate/{id}",
		MovieUpdate,
	},
	{
		"MovieDelete",
		"DELETE",
		"/peliculadelete/{id}",
		MovieDelete,
	},
}