package main

import "fmt"

var vector [3]string //Como declarar el array
var vector1 []string //Como declarar el slice
var vector2 [][]string //Como declarar el slice
//La diferencia entre un slice y un array es que el array tiene una dimension fija, el slice no
func main()  {
	vector = [3]string{"ciudadano ejemplar", "star wars", "hombre en llamas"}
	vector1 = []string{"ciudadano ejemplar", "star wars", "hombre en llamas", "hola", "mundo", "mundanal"}
	vector2 = [][]string{{"ciudadano ejemplar", "star wars", "hombre en llamas", "hola", "mundo", "mundanal"}, {"slice", "multidimensional", "hola", "mundo"}}
	fmt.Println(vector)
	fmt.Println(vector1)
	fmt.Println(vector2)
}


