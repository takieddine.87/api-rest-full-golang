package main

import "fmt"

func gorras(pedido float64) (string, float64)  {
	precio := func() float64{
		return pedido * 7
	}()
	return "El precio de las gorras pedidas es: ", precio
}

func main()  {
	fmt.Println(gorras(45))
}