package main

import (
	"fmt"
	"math"
	"time"
)

func main()  {
	fmt.Println("****** MI PROGRAMA CON GO ******")

	birthday := time.Date(1987, 10, 24, 0,0,0,0, time.UTC)
	today := time.Now()
	fmt.Println(math.Floor(today.Sub(birthday).Hours() / 24 / 365), "anios")
	edad := math.Floor(today.Sub(birthday).Hours() / 24 / 365)
	if edad > 18 {
		fmt.Println("Puedes pasar")
	} else {
		fmt.Println("No puedes pasar")
	}
}