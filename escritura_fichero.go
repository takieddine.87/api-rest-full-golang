package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func main()  {
	fmt.Println("Escritura de ficheros")
	nuevo_texto := []byte("Go is an open source programing language that makes it easy to build simple,reliable,and efficient software")
	//escribir := ioutil.WriteFile("ficheros.txt", nuevo_texto, 0777)
	//showErrors(escribir)
	/*fichero, err := os.OpenFile("ficheros.txt", os.O_APPEND, 0777)
	if err != nil {
		panic(err)
	}
	escribir, err := fichero.WriteString(nuevo_texto)
	fmt.Println(escribir)
	fichero.Close()*/
	fichero, err := os.OpenFile("ficheros.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0777)
	showErrors(err)

	escribir, err := fichero.WriteString(string(nuevo_texto))
	fmt.Println(escribir)
	showErrors(err)

	fichero.Close()

	fichero1, err := ioutil.ReadFile("ficheros.txt")
	showErrors(err)
	fmt.Println(string(fichero1))
}

func showErrors(e error)  {
	if e != nil {
		panic(e)
	}
}
