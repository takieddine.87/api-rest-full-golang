package main

import (
	"fmt"
	"io/ioutil"
)

func main()  {
	fmt.Println("Lector de ficheros")

	fichero, err := ioutil.ReadFile("ficheros1.txt")
	showError(err)
	fmt.Println(string(fichero))
}

func showError(e error)  {
	fmt.Println("Tienes un error")
	if e != nil {
		panic(e)
	}
}