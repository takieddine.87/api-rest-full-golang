package main

import (
	"fmt"
)

func intScanln(n int) ([]int, error) {
	x := make([]int, n) //Sirve para construir el slice de antemano [0 0 0]
	y := make([]interface{}, len(x)) //Output: [<nil> <nil> <nil>]
	fmt.Println(x)
	fmt.Println(y)
	for i := range x {
		y[i] = &x[i]
	} //Le asignamos a cada valor de 'y' un valor con la direccion de memoria de 'x'
	n, err := fmt.Scanln(y...)
	fmt.Println(n)
	x = x[:n]
	return x, err
}

func retornoSlice() {
	var array [50]int
	var total, count int
	fmt.Print("How many numbers you want to enter: ")
	fmt.Scanln(&count)
	for i := 0; i < count; i++ {
		fmt.Print("Enter value : ")
		fmt.Scanln(&array[i])
		total += array[i]
	}
	fmt.Println(total)
}

func main() {
	retornoSlice()
	x, err := intScanln(3)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Printf("%v\n", x)
}