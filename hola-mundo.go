package main

import (
	"fmt"
)

// main para ejecutar concatenacion
func main() {
	const year int = 2020
	// year = 2018 //No se puede hacer ya que year es una constante
	var frase string = "Hello"
	fmt.Println("Hola de aqui " + frase)
	fmt.Printf(frase)
}
