package main

import "fmt"

type Gorra struct {
	marca string
	color string
	precio float32
	plana bool
}

func main()  {
	gorraNegra := Gorra{
		marca: "nike",//Siempre comas dobles
		color: "negro",
		precio: 25.50,
		plana: false,
	}
	gorraRoja := Gorra{"Adidas", "Roja", 24.90, true}
	fmt.Println(gorraNegra)
	fmt.Println(gorraRoja)
}