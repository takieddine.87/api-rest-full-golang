package main

import (
	"fmt"
	"time"
)

func main()  {
	momento := time.Now()
	hoy := momento.Weekday()

	switch hoy {
	case 0:
		fmt.Println("Hoy es domingo")
	case 1:
		fmt.Println("Hoy es Lunes")
	case 5:
		fmt.Println("Hoy es viernes")
	default:
		fmt.Println("Es otro dia de la semana")
	}
}
